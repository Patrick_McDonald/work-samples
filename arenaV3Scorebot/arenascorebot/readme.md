# Introduction
    The following code sample is the scoring bot that was used as part of the Mei Force Arena workshop at the Boston Area Security Conference.


# What is the MEI Force Arena
    The Mei force arena is the name of an infastructure system and lesson plan to be used in Red vs blue team training. Specificly the system is designed to gamify the training by presenting it in the form of a battle royal style capture the flag game. 
    The infastructure consists of an Amazon web services Virtual Private Cloud containing between 3 to 8 subnets based on the setup configuration. Each of the subnets have 4 to 10 ec2 instances running vunerable web application of various sorts.
    Unlike many similar events which use flag based scoring were each flag is worth a certian number of points, the Mei Force arena uses a king of the hill style scoring where teams score points based on how many flags they set to their team name. The flags are web facing text files hidden on each server
    Wholely unique to the Mei Force arena the teams of contestents are divided into two types. The first are teams solly devoted to offense and only score by taking over targets. The second type is defensive, in addition to gaining points by taking over targets, each defensive team is given full access to all of the targets on one of the subnets and gains points by preventing other players from capturing flags on them.

# About the scorebot
