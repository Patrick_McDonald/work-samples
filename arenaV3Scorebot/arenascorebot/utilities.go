package main

import (
	//	"
//	"fmt"
	"math/rand"
	"time"
	"strconv"
	"io/ioutil"
	"log"
	"path/filepath"
	"text/template"
	"golang.org/x/crypto/ssh"
	"os"
)

func Shuffle(vals []host) []host {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	ret := make([]host, len(vals))
	perm := r.Perm(len(vals))
	for i, randIndex := range perm {
		ret[i] = vals[randIndex]
	}
	return ret
}

func ShuffleVms(vals []vmInformation) []vmInformation {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	ret := make([]vmInformation, len(vals))
	perm := r.Perm(len(vals))
	for i, randIndex := range perm {
		ret[i] = vals[randIndex]
	}
	return ret
}
func Shufflefilespaths(vals []string) []string {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	ret := make([]string, len(vals))
	perm := r.Perm(len(vals))
	for i, randIndex := range perm {
		ret[i] = vals[randIndex]
	}
	return ret
}
func (configdata configurationData)getVmByIdNumber(idnumber int)host{
	for _,value :=range configdata.Hosts{
		if value.Idnumber==idnumber{
			return value
		}
	}
	return host{"",0}
}

func generateTemplateFile(programconfig configurationData,virtualmachines []vmInformation){
	templatefile, err := os.Create("template.html")
	scoringpageTemplate, err := template.ParseFiles("rawtemplate.html")
	if err != nil {
		log.Println(err.Error())
	}
	scoresTable := ""
	for _, value := range programconfig.Teamsnames {
		//fmt.Println(value)
		scoresTable = scoresTable + "<tr><td><a href=/TeamScoreFiles/" + value + "/index.txt>" + value + "</a></td><td>{{." + value + "attackscore}}</td><td>{{." + value + "scoredefense}}</td></tr>"
	}
	scoringBreakdown := "<tr><th>Team</ht>"
	for _, vm := range virtualmachines {
		//fmt.Println(vm.Idnumber)
		scoringBreakdown = scoringBreakdown + ("<th>" + strconv.Itoa(vm.Idnumber) + "</th>")
	}
	scoringBreakdown = scoringBreakdown + "</tr>"
	for _, host := range programconfig.Teamsnames {
	//	fmt.Println(host)
		scoringBreakdown = scoringBreakdown + "<tr><td>" + host + "</td>"
		for _, vm := range virtualmachines {
	//		fmt.Println(vm.Idnumber)
			scoringBreakdown = scoringBreakdown + ("<td>{{." + host + "vm" + strconv.Itoa(vm.Idnumber) + "rounddefensescore}}<br/>{{." + host + "vm" + strconv.Itoa(vm.Idnumber) + "roundattackscorers}}</td>")
		}
		scoringBreakdown = scoringBreakdown + "</tr>"
	}
	templateHtmlData := make(map[string]string, 0)
	filedata,err:=ioutil.ReadFile("topscorerstables.txt")
	templateHtmlData["topscorers"] =string(filedata)
//	fmt.Println(scoresTable)
//	fmt.Println(scoringBreakdown)
	templateHtmlData["scoretable"] = scoresTable
	templateHtmlData["scoringBreakdown"] = scoringBreakdown
	err = scoringpageTemplate.Execute(templatefile, templateHtmlData)
	if err != nil {
		log.Println(err.Error())
	}
}



func createmachienpublickeypairs(virtualmachines []vmInformation)map[int]ssh.PublicKey{
	var hostauthkeys map[int]ssh.PublicKey
	hostauthkeys=make(map[int]ssh.PublicKey)
	for _,vm :=range virtualmachines{
		hostauthkeys[vm.Idnumber],_=ssh.ParsePublicKey([]byte(vm.SshhostKey))
	}
	return hostauthkeys
}
func RemoveContents(dir string) error {
    d, err := os.Open(dir)
    if err != nil {
        return err
    }
    defer d.Close()
    names, err := d.Readdirnames(-1)
    if err != nil {
        return err
    }
    for _, name := range names {
        err = os.RemoveAll(filepath.Join(dir, name))
        if err != nil {
            return err
        }
    }
    return nil
}
