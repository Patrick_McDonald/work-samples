package main
import ("fmt"
  
	"io/ioutil"
	"encoding/json"
	)
func loadScorebotConfiguration(configurationFile string){
	rawfiledata,mainCOnfigerror:=ioutil.ReadFile(configurationFile)
	json.Unmarshal(rawfiledata,&ProgramCOnfigurationData)
	if mainCOnfigerror!=nil{
		fmt.Println("error reading config file "+mainCOnfigerror.Error())
	}
}
func loadvirtualmachineInformation(virtualMachineFile string)[]vmInformation{
	//programConfigData :=ProgramConfiguration{}
	rawfiledata,machineDataError:=ioutil.ReadFile(virtualMachineFile)
	var vmsData []vmInformation
	unmarshalerror:=json.Unmarshal(rawfiledata,&vmsData)
	if machineDataError!=nil{
		fmt.Printf("error opening vm file "+machineDataError.Error())
	}
        if  unmarshalerror!=nil{
                fmt.Printf("error unmarshalling data "+ unmarshalerror.Error())
        }

	return vmsData
}
