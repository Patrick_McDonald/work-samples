package main

type scoreRecord struct {
	teamname     string
	defenseScore int
	attackscore  int
}
type eventsScores struct {
	teams []scoreRecord
}

func (e eventsScores) addTeam(name string, ascore int, dscore int) {
	e.teams = append(e.teams, scoreRecord{name, ascore, dscore})
}

func (e eventsScores) getScoreRecords() []scoreRecord {
	return e.teams
}
func (e eventsScores) incrementteamattackscore(name string) {
	for index, team := range e.teams {
		if team.teamname == name {
			e.teams[index].attackscore++
		}
	}
}
func (e eventsScores) incrementteamdefensescore(name string) {
	for index, team := range e.teams {
		if team.teamname == name {
			e.teams[index].defenseScore++
		}
	}
}
func (e eventsScores) sortbyattackscore() []scoreRecord {
	a := e.teams
	for i := 1; i < len(a); i++ {
		value := a[i]
		j := i - 1
		for j >= 0 && a[j].attackscore < value.attackscore {
			a[j+1] = a[j]
			j = j - 1
		}
		a[j+1] = value
	}
	return a
}
func (e eventsScores) sortbydefendersscore() []scoreRecord {
	a := e.teams
	for i := 1; i < len(e.teams); i++ {
		value := a[i]
		j := i - 1
		for j >= 0 && a[j].defenseScore < value.defenseScore {
			a[j+1] = a[j]
			j = j - 1
		}
		a[j+1] = value
	}
	return a
}
