package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/crypto/ssh"
)

type host struct {
	Name     string
	Idnumber int
}
type configurationData struct {
	Teamsnames                []string
	Hosts                     []host
	Masterscoringfrequency    float32
	Vminformationfilepath     string
	EventEndTime              time.Time
	Logfilename               string
	HttpClientDuration        int
	ScoreRecordFilepath       string
	Httpdroot                 string
	Scorepagetemplatetemplate string
}
type vmInformation struct {
	Name       string
	Idnumber   int
	Scorefiles []string
	SshhostKey string
}

var hostauthkeys = make(map[int]ssh.PublicKey)
var thisEventScores eventsScores
var virtualMachines []vmInformation

const subnet = "172.17"

var numberofhosts int
var numberofvms int
var isRunning bool
var scoringWaitGroup sync.WaitGroup
var mainWaitGroup sync.WaitGroup
var scoringIsRunning bool
var logwriter *log.Logger
var scorebotHttpClient = http.Client{
	Timeout: time.Second * 15,
	//CheckRedirect: func(){},
}

//Setup the ssh client
var sshconfig = &ssh.ClientConfig{
	User: "root",
	HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		host := remote.String()
		address := strings.Split(host, ":")[0]
		vm := strings.Split(address, ".")[3]
		vmmachineid, _ := strconv.Atoi(vm)
		if bytes.Compare(key.Marshal(), hostauthkeys[vmmachineid].Marshal()) == 0 {
			return nil
		}
		return errors.New("host key mismatch")
	},
}

var ProgramCOnfigurationData configurationData

func main() {
	scorebotconfig := flag.String("config", "./scorebotconfig.json", "scorebot configuration file")
	InstanceConfig := flag.String("Instances", "./vminfo.json", "information On the VM Instances")
	flag.Parse()
	loadScorebotConfiguration(*scorebotconfig)
	virtualMachines = loadvirtualmachineInformation(*InstanceConfig)
	hostauthkeys = createmachienpublickeypairs(virtualMachines)
	generateTemplateFile(ProgramCOnfigurationData, virtualMachines)
	key, err := ioutil.ReadFile("/home/ubuntu/.ssh/id_rsa")

	if err != nil {
		log.Fatalf("unable to read private key: %v", err)
	}

	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	sshconfig.Auth = []ssh.AuthMethod{ssh.PublicKeys(signer)}

	if _, err := os.Stat("./archive"); err != nil {
		os.Mkdir("archive", 777)
	}

	/*

	 */
	previousdata, _ := ioutil.ReadDir("./scorerecord")
	for _, file := range previousdata {
		temp, _ := ioutil.ReadFile("./scorerecord/" + file.Name())
		ioutil.WriteFile("./archive/"+file.Name(), temp, 777)
		os.Remove("./scorerecord/" + file.Name())
	}

	fmt.Println("Scorebot starting up")
	fmt.Print("the time is ")
	log.Println(time.Now())
	isRunning = true
	file, err := os.OpenFile(ProgramCOnfigurationData.Logfilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {

	}

	/*
		In the arena, teams can score in two ways.
		 Gain one point each scoring cycle for
		 every scorefile they control in their
		 home system or one point for every score file
		 they contol in another teams system. The following
		 block initalizes the scores for all teams
	*/
	for _, value := range ProgramCOnfigurationData.Teamsnames {
		thisEventScores.teams = append(thisEventScores.teams, scoreRecord{value, 0, 0})
	}
	defer file.Close()
	log.SetFlags(log.LstdFlags)
	log.SetOutput(file)
	fmt.Print("the time is ")
	fmt.Println(time.Now())
	fmt.Print("Configuration complete press the enter key to start scoring")
	reader := bufio.NewReader(os.Stdin)
	reader.ReadString('\n')
	//setting the intial run time to allow calculation of the
	//time for the next scoring run
	timeoflastscoringrun := time.Date(0, 11, 0, 0, 0, 0, 0, time.UTC)
	var numberOfScoringRuns int = 0
	var nextScoringRun time.Time = time.Now()
	for {
		currentTime := time.Now()
		if !currentTime.Before(ProgramCOnfigurationData.EventEndTime) {
			isRunning = false
			fmt.Println("Times up, the event is over, scoring shutting down")
			os.Exit(1)
			break

		}
		currenttime := time.Now()
		if !currenttime.Before(nextScoringRun) {
			scoringLoop()
			numberOfScoringRuns = numberOfScoringRuns + 1
			fmt.Println("scoring run :", numberOfScoringRuns)
			nextScoringRun = timeoflastscoringrun.Add(time.Minute * time.Duration(ProgramCOnfigurationData.Masterscoringfrequency))
			fmt.Println("next scoring run", nextScoringRun)
			timeoflastscoringrun = currenttime
		}
	}
}

func scoringLoop() {

	log.Println("starting scoring")
	/*
		The following block of code creates 

	*/
	scoringWaitGroup.Add(len(virtualMachines) * len(ProgramCOnfigurationData.Hosts) * 5)
	for _, host := range Shuffle(ProgramCOnfigurationData.Hosts) {
		for _, virutalmachine := range ShuffleVms(virtualMachines) {

			ipaddress := subnet + "." + strconv.Itoa(host.Idnumber) + "." + strconv.Itoa(virutalmachine.Idnumber)
			for _, filepath := range Shufflefilespaths(virutalmachine.Scorefiles) {
				protocolandfilepath := strings.Split(filepath, " ")
				log.Println("reading", protocolandfilepath[0]+ipaddress+protocolandfilepath[1])
				go readScoreFile(protocolandfilepath[0] + ipaddress + protocolandfilepath[1])
			}
		}
	}
	scoringWaitGroup.Wait()
	log.Println("updateScorepage")
	updateScorepage()
	RemoveContents(ProgramCOnfigurationData.ScoreRecordFilepath)
}

func updateScorepage() {
	/*
		This section of code is largely an adapation of
		of code from the original scorebot used at BASC
		2017 which was written by my boss in bash script.
		I kept the original design mainly to prevent
		attempts by the contestants to hack into this piece
		of software, The following function extracts data
		from the current score and the saved state of each
		scorefiles and renders that data into a previously
		generated template of the scoreboard. This code has
		been removed from the current version of the scorebot
		and replaced with a minimal api.
	*/
	templateData := make(map[string]string)
	currentrundefenseScoresincreases := make(map[string]int)
	currentrunvmattackerrecord := make(map[string]string)
	scorerecordfiles, error := ioutil.ReadDir(ProgramCOnfigurationData.ScoreRecordFilepath)
	
	for _, scorerecordfileinfo := range scorerecordfiles {
		scorefilename := scorerecordfileinfo.Name()
		scorefileInfo := strings.Split(scorefilename, "_")
		scorerecordfilecontents, readerror := ioutil.ReadFile(ProgramCOnfigurationData.ScoreRecordFilepath + scorefilename)
		if readerror != nil {
			log.Println(readerror.Error())
		}
        //
		cleanedfilecontents := strings.TrimRight(string(scorerecordfilecontents), "\n")
		if cleanedfilecontents == scorefileInfo[0] {
			currentrundefenseScoresincreases[scorefileInfo[0]+"-"+scorefileInfo[1]]++
			thisEventScores.incrementteamdefensescore(string(cleanedfilecontents))
		} else {
			thisEventScores.incrementteamattackscore(string(cleanedfilecontents))
			currentrunvmattackerrecord[scorefileInfo[0]+"-"+scorefileInfo[1]] = currentrunvmattackerrecord[scorefileInfo[0]+"-"+scorefileInfo[1]] + string(scorerecordfilecontents) + " "
		}
		create_team_scoring_breakdown(scorefilename, cleanedfilecontents)
	}
	for _, value := range thisEventScores.getScoreRecords() {
		log.Println(value.teamname + "attack score " + strconv.Itoa(value.attackscore))
		templateData[value.teamname+"attackscore"] = strconv.Itoa(value.attackscore)
		log.Println(value.teamname + "defense score " + strconv.Itoa(value.defenseScore))
		templateData[value.teamname+"scoredefense"] = strconv.Itoa(value.defenseScore)
	}
	places := []string{"nil", "top", "second", "third"}
	countofplace := 0
	attackertopscorers := thisEventScores.sortbyattackscore()
	defenderstopscorers := thisEventScores.sortbydefendersscore()
	//get the top attackers and defenders and insert them into the template data map
	for _, value := range attackertopscorers {
		countofplace++
		if countofplace > 3 {
			break
		}
		templateData[places[countofplace]+"placeattacker"] = value.teamname
		templateData[places[countofplace]+"placeattackerattackscore"] = strconv.Itoa(value.attackscore)
		templateData[places[countofplace]+"placeattackerdefensescore"] = strconv.Itoa(value.defenseScore)
	}
	countofplace = 0
	for _, value := range defenderstopscorers {
		countofplace++
		if countofplace > 3 {
			break
		}
		templateData[places[countofplace]+"placedefender"] = value.teamname
		templateData[places[countofplace]+"placedefenderattackscore"] = strconv.Itoa(value.attackscore)
		templateData[places[countofplace]+"placedefenderdefenderscore"] = strconv.Itoa(value.defenseScore)
	}

	log.Println("defense score increases", currentrundefenseScoresincreases)
	for machinevmkey, incrementamount := range currentrundefenseScoresincreases {
		if incrementamount > 0 {
			hostandvm := strings.Split(machinevmkey, "-")
			templateData[hostandvm[0]+"vm"+hostandvm[1]+"rounddefensescore"] = "+" + strconv.Itoa(incrementamount)
		}
	}
	//
	
	log.Println("whoscored where increases", currentrunvmattackerrecord)
	for machinevmkey, namesofscoringteams := range currentrunvmattackerrecord {
		hostandvm := strings.Split(machinevmkey, "-")
		templateData[hostandvm[0]+"vm"+hostandvm[1]+"roundattackscorers"] = namesofscoringteams
	}
	log.Println("scoring run results")
	log.Println("scores", thisEventScores.getScoreRecords())
	log.Println("template data", templateData)
	
	
	//remove the old scorepage and create a new page to render the scoreboard to
	os.Remove(ProgramCOnfigurationData.Httpdroot + "index.html")
	scoreHtmlOutputPage, templaterror := os.Create(ProgramCOnfigurationData.Httpdroot + "index.html")
	fmt.Println(ProgramCOnfigurationData.Httpdroot)
	
    //render the template
	scoringpageTemplate, templateloaderror := template.ParseFiles("template.html")
	if templateloaderror != nil {
		log.Println("templateparserror" + templateloaderror.Error())

	}

	if templaterror != nil {
		log.Println(templaterror.Error())
	}
	if error != nil {
		log.Println(error.Error())
	}

	ExecutionError := scoringpageTemplate.Execute(scoreHtmlOutputPage, templateData)
	if ExecutionError != nil {
		fmt.Println("templateexecutionerr0r" + ExecutionError.Error())

	}
	error = scoreHtmlOutputPage.Sync()
	scoreHtmlOutputPage.Close()
	if error != nil {
		log.Println(error.Error())
	}

}
func readScoreFile(url string) {
	/*
		My boss requested that scorefiles be readable by either
		ssh or http. The inclusion of ssh was to allow for
		scorefiles to be in non web accessable directoreis
		requiring more creative attacks on the part of the
		contenestents
	*/
	httpRegex, _ := regexp.Compile("http://")
	sshregex, _ := regexp.Compile("ssh://")
	if httpRegex.MatchString(url) {
		readScorehttpFile(url)
	} else if sshregex.MatchString(url) {
		readScoreFileViaSsh(url)
	}
}

/*
	the following two functions use another holdover from
	the original scorebot. Both functions saved the values
	they read from the scorefiles to files on the hard drive.
	There are two reasons for this, the first is that  my
	boss requested this feature.
	The second reason was security, given the nature of the
	application, it was reasonable to expect some of the contestents
	would at least try to hack the server the code was running on
	which meant that using most forms of database software was
	going to be a difficult. Prior to the event we would have to
	make sure the database was button down tight and up to date,
	even then it would not be garnteeded to work. Using static
	files ensured that no one could hack the score record.
*/

func readScoreFileViaSsh(url string) {
	//extract the ip address from url and connect to server via ssh
	host := strings.Replace(url, "ssh://", "", 1)
	filepath := strings.SplitN(host, "/", 2)
	sshconn, connectionerror := ssh.Dial("tcp", filepath[0], sshconfig)
	if connectionerror != nil {
		log.Println(connectionerror.Error())
	}
	session, sessionerror := sshconn.NewSession()
	if sessionerror != nil {
		fmt.Println(sessionerror.Error())
	}
	defer session.Close()
	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	//run less to get the file contents
	session.Run("less " + filepath[1])
	//the following code extracts the host index
	//, the scorefile name and uses it to create
	// the filepath for the score record
	dottedURL := strings.Replace(strings.Replace(filepath[0], ":22", "", 25), "/", ".", 25)
	fileelemetns := strings.Split(strings.Replace(dottedURL, "/", ".", 60), ".")
	log.Println(fileelemetns)
	hostindex, _ := strconv.Atoi(fileelemetns[2])
	scorefilename := fileelemetns[len(fileelemetns)-2]
	filename := ProgramCOnfigurationData.getVmByIdNumber(hostindex).Name + "_" + fileelemetns[3] + "_" + scorefilename
	scoringfile := ProgramCOnfigurationData.ScoreRecordFilepath + filename
	log.Println("scoring file filepath", scoringfile)
	parsedfilecontents := stdoutBuf.String()
	//Check if the contents are longer 20 characters long
	//to prevent injection attacks
	if len(parsedfilecontents) < 25 {
		scorerecord, err := os.Create(scoringfile)
		scorerecord.WriteString(parsedfilecontents)
		scorerecord.Close()
		if err != nil {

		}
	}
}

func readScorehttpFile(url string) {
	req, _ := http.NewRequest("GET", url, nil)
	log.Println("reading " + url)
	machineresponse, httpError := scorebotHttpClient.Do(req)

	if httpError == nil {

		filecontents, httpError := ioutil.ReadAll(machineresponse.Body)
		log.Println(url + " " + string(filecontents))
		machineresponse.Body.Close()

		//the following code extracts the host index
		//, the scorefile name and uses it to create
		// the filepath for the score record
		dottedURL := strings.Replace(strings.Replace(url, "http://", "", 25), "/", ".", 25)
		fileelemetns := strings.Split(strings.Replace(dottedURL, "/", ".", 60), ".")
		log.Println(fileelemetns)
		hostindex, _ := strconv.Atoi(fileelemetns[2])
		scorefilename := fileelemetns[len(fileelemetns)-2]
		vmname := fileelemetns[3]
		containsPortNumber, _ := regexp.MatchString(":[0-9]+", vmname)
		vmnamesantized := ""
		if containsPortNumber {
			vmnamesantized = strings.Split(vmname, ":")[0]
		} else {
			vmnamesantized = vmname

		}
		filename := ProgramCOnfigurationData.getVmByIdNumber(hostindex).Name + "_" + vmnamesantized + "_" + scorefilename
		scoringfile := ProgramCOnfigurationData.ScoreRecordFilepath + filename
		log.Println("scoring file filepath", scoringfile)
		parsedfilecontents := string(filecontents)
		//Check if the contents are longer 10 characters long
		//to prevent injection attacks(all team names where shorter than ten characters)
		if len(parsedfilecontents) < 10 {
			scorerecord, err := os.Create(scoringfile)
			scorerecord.WriteString(parsedfilecontents)
			scorerecord.Close()
			if err != nil {

			}
		}

		if httpError != nil {
			log.Println(httpError.Error())
		}
	}
	if httpError != nil {
		log.Println(httpError.Error())
	}
	scoringWaitGroup.Done()

}

//The following code updates a web accessable blow by blow record of the tean's score
// this feature has been removed from the current version of the code
func create_team_scoring_breakdown(filename, filecontents string) {
	teamfile, err := os.OpenFile("var/www/html/TeamScoreFiles/"+filecontents+"/index.txt", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {

	}
	filenameparts := strings.Split(filename, "_")
	if filenameparts[0] == filecontents {
		teamfile.WriteString("At " + time.Now().String() + " team " + filecontents + " scored 1 point for defending vm " + filenameparts[1] + " " + filenameparts[2] + " on machine " + filenameparts[0])

	} else {
		teamfile.WriteString("At " + time.Now().String() + " team " + filecontents + " scored 1 point for capturing vm " + filenameparts[1] + " " + filenameparts[2] + " on machine" + filenameparts[0])

	}
}
